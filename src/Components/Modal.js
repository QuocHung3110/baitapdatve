import React, { Component } from "react";

export default class Modal extends Component {
  
  render() {
    let { image, name, price, description, shortDescription } =
      this.props.detail;
    return (
      // <div className="row">
      //   <div className="col-4">
      //     <img src={image}/>
      //   </div>
      //   <div className="col-8">
      //     <h4>{name}</h4>
      //     <p>Price: ${price}</p>
      //     <p>{description}</p>
      //     <p>{ shortDescription}</p>

      //   </div>

      // </div>
      
      <div>
        <div className="modal" tabIndex={-1} role="dialog" id="myModal">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title">{name}</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">×</span>
                </button>
              </div>
              <div className="modal-body">
                <img src={image} style={{width:300}}/>
                <p>${price}</p>
                <p>{description}</p>
                <p>{shortDescription}</p>
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary"
                  data-dismiss="modal"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
