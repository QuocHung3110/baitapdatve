import React, { Component } from "react";
import Cart from "./Cart";
import ItemShoe from "./ItemShoe";

export default class ProductList extends Component {
  render() {
    return (
      <div className="row">
        {/* danh sach giay */}
        <div className="row col-6" id="productList">
          {this.props.productsData.map((item) => {
            return <ItemShoe shoe={item} addToCard={this.props.addToCard} detail={this.props.detail}/>;
          })}
        </div>
        {/* danh sach cart */}
        <div className="col-6" id="cart">
        {this.props.cart.length>0 && <Cart
            cart={this.props.cart}
            productsData={this.props.productsData}
            delete={this.props.delete}
            changeQuantity={this.props.changeQuantity}
          />}
        </div>
      </div>
    );
  }
}
