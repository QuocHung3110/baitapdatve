import React, { Component } from "react";

export default class Cart extends Component {
  renderTableCart = () => {
    return this.props.cart.map((item) => {
      return (
        <tr>
          <td>
            <img src={item.image} style={{ width: 50 }} />
          </td>
          <td>{item.name}</td>
          <td>
            <button
              className="btn btn-outline-danger"
              onClick={() => {
                this.props.changeQuantity(item.id,-1);
              }}
            >
              <i class="bi bi-dash"></i>
            </button>
            <span className="mx-2">{item.soLuong}</span>
            <button
              className="btn btn-outline-success"
              onClick={() => {
                this.props.changeQuantity(item.id,1);
              }}
            >
              <i class="bi bi-plus"></i>
            </button>
          </td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              className="btn btn-outline-danger"
              onClick={() => {
                this.props.delete(item.id);
              }}
            >
              <i class="bi bi-trash3-fill"></i>
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <h3 className="text-info">Your Cart</h3>
        <table className="table table-striped">
          <thead>
            <tr>
              <td>Image</td>
              <td>Name</td>
              <td>Quantity</td>
              <td>Price</td>
              <td>Action</td>
            </tr>
          </thead>
          <tbody>{this.renderTableCart()}</tbody>
        </table>
      </div>
    );
  }
}
