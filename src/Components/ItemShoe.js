import React, { Component } from "react";

export default class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.shoe;
    return (
      <div className="col-4 mb-4">
        <div className="card">
          <img className="card-img-top" src={image} alt="Card image cap" />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <p className="card-text">$ {price}</p>
            <button
              className="btn btn-outline-info mr-2"
              onClick={() => this.props.addToCard(this.props.shoe)}
            >
              Add to cart <i class="bi bi-bag-heart-fill"></i>
            </button>
            <button className="btn btn-outline-secondary" onClick={()=>{this.props.detail(this.props.shoe)}} data-toggle="modal" data-target="#myModal">
              Detail <i class="bi bi-bag-heart-fill"></i>
            </button>
          </div>
        </div>
      </div>
    );
  }
}
