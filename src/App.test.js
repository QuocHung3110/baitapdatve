import { render, screen } from '@testing-library/react';
import ShoesStore from './ShoesStore';

test('renders learn react link', () => {
  render(<ShoesStore />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
